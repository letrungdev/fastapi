from datetime import time, datetime

from .database import Base
from sqlalchemy import Boolean, Column, Integer, String

class Video(Base):
    __tablename__ = "video"

    id = Column(Integer, primary_key=True, index=True)
    category_id = Column(list, null=True)
    video_name = Column(dict, null=True)
    video_url = Column(String(100), null=True)
    video_name_cn = Column(String(100), null=True)
    video_name_tw = Column(String(100), null=True)
    channel_id = Column(Integer, null=True)
    image_url = Column(String(100), null=True)
    number_like = Column(Integer, null=True)
    total_time = Column(time, null=True)
    video_start_time = Column(time, null=True)
    video_end_time = Column(time, null=True)
    level = Column(list, null=True)
    grammar_id = Column(Integer, null=True)
    check_premium = Column(Boolean, null=True)
    video_temp = Column(Boolean, null=True)
    created_at = Column(datetime)
    trial_end = Column(Integer, null=True)
    trial_start = Column(Integer, null=True)

class Quiz(Base):
    __tablename__ = "quiz"

    video_id = Column(Integer, null=True)
    category_id = Column(Integer, null=True)
    quiz_question = Column(String(100), null=True)
    quiz_answer_correct = Column(String(100), null=True)
    quiz_data_answer = Column(dict, null=True)
    quiz_kind = Column(String(100), null=True)
    quiz_translate = Column(dict, null=True)
    quiz_question_tw = Column(String(100), null=True)
    quiz_answer_correct_tw = Column(String(100), null=True)
    quiz_data_answer_tw = Column(list, null=True)

class Category(Base):
    __tablename__ = "category"

    id = Column(Integer, primary_key=True, index=True)
    category_name = Column(dict)

class Channel(Base):
    __tablename__ = "channel"
    id = Column(Integer, primary_key=True, index=True)
    videoid = Column(list, null=True)
    name_channel = Column(String(100), null=True)
    url_channel = Column(String(100), null=True)
    image_channel = Column(String(100), null=True)
    description_channel = Column(String(100), null=True)

class Grammar(Base):
    __tablename__ = "grammar"
    id = Column(Integer, primary_key=True, index=True)
    grammar_title = Column(String(100), null=True)
    grammar_level = Column(String(100), null=True)
    use_for = Column(String(100), null=True)
    grammar_content = Column(String(100), null=True)
    grammar_keys = Column(String(100), null=True)
    grammar_example = Column(list, null=True)
    grammar_vi = Column(String(100), null=True)
    grammar_en = Column(String(100), null=True)

class Level(Base):
    __tablename__ = "level"
    id = Column(Integer, primary_key=True, index=True)
    level_name = Column(dict)

class Subtitle(Base):
    __table_name__ = "subtitle"
    id = Column(Integer, primary_key=True, index=True)
    video_id = Column(Integer, null=True)
    oder_id = Column(Integer, null=True)
    quiz_id = Column(Integer, null=True, null=True)
    sentence_value = Column(String(100), null=True)
    sentence_pronounce = Column(String(100), null=True)
    sentence_trans = Column(String(100), null=True)
    start_time = Column(time, null=True)
    end_time = Column(time, null=True)
    sentence_value_tw = Column(String(100), null=True)



